jQuery(document).ready(function($) {

    $('.dropdown-toggle').dropdown();

    $('.my_table').dataTable({
                       responsive: true,
                       "aaSorting": [[0, "asc"]],
                       oLanguage: {
                         sLengthMenu: "Afficher: _MENU_ lignes par page ",
                         sSearch: "Rechercher : ",
                         sZeroRecords: "Aucune valeur trouvée !!",
                         sInfo: "page _PAGE_ sur _PAGES_",
                         sInfoFiltered: "(Filtrés sur _MAX_ lignes)",
                         sInfoEmpty: "Aucun élément trouvé",
                       oPaginate: {
                         sFirst: "Premier ",
                         sPrevious: "  Precedent  ",
                         sNext: "  Suivant ",
                         sLast: "  Dernier"
                      }
                    },
                     "pagingType": "full_numbers"
                 });


 $(window).load(function(){

    if($('#idIsAddProfessor').val() == 'true'){
       $('#add_professor').modal();
    }

    if($('#idIsAddUser').val() == 'true'){
           $('#add_user').modal();
    }

     if($('#idIsAddSalary').val() == 'true'){
           $('#add_salary').modal();
     }

    if($('#idIsAddSchoolPeriod2').val() == 'true'){
            $('#add_school_period').modal();
    }

     if($('.hasFieldErrorsConfirmPassword').val() == 'true') {
       $('.msgErrorConfirmPassword').removeClass('hidden');
       $('#update_password').modal();
     }

     if($('.hasFieldErrorsLastPassword').val() == 'true') {
            $('.msgErrorLastPassword').removeClass('hidden');
            $('#update_password').modal();
     }

     if($('#idIsAddMark').val() == 'true') {
       $('#add_mark').modal();
     }

      if($('#idIsAddClasse').val() == 'true'){
            $('#add_classe').modal();
            //alert('okkkkkkkkkkkk');
         }

      if($('#idIsAddSubjects').val() == 'true'){
        $('#add_subjects').modal();
        //alert('okkkkkkkkkkkk');
      }

      if($('#idIsAddCoef').val() == 'true'){
        $('#add_coef').modal();
      }

      if($('#idIsStudentRank').val() == 'true'){
          $('#student_rank').modal();
      }

      if($('#idIsAddInscription').val() == 'true'){
          $('#add_inscription').modal();
      }

      if($('#idIsEditInscription').val() == 'true'){
          $('#edit_inscription').modal();
      }

 });



   /*$("#tbodyClientTable tr").click(function() {
        var value = $(this).attr('id');
        if(!isNaN(value)) {
           location.href = "/client/" + value;
       }
   }); */

// Professor
 $('.editProfessor').click(function(){
    var id = $(this).attr('data');
    getProfessorToEdit(id);
    $('#add_professor').modal();
 });

 $('#btnAddProfessor').click(function(){
   $('#formAddProfessor input').val('');
   $('#formAddProfessor select').val('');
 });

/* $('.checkedSalaryPayment').change(function(){
     if($(this).is(":checked")){
       $(this).val('checked');
     }else{
       $(this).val('no');
     }
 }); */

$('.deleteProfessor').click(function(){
   var id = $(this).attr('data');
  $('#btnDeleteProfessor').attr("href",'/delete/professor/'+id);
  $('#delete_professor').modal();
});

$('.profPayment').click(function(){
    var id = $(this).attr('data');
    getProfessorToPaid(id);
    $('#add_prof_payment').modal();
});

$('.editProfPayment').click(function(){
    var id = $(this).attr('data');
    getProfessorPaidToEdit(id);
    $('#add_prof_payment').modal();
});

$('.detailsProfPayment').click(function(){
    var id = $(this).attr('data');
    getProfPayment(id);
    $('#details_prof_payment').modal();
});

// Horary Statement
$('.deleteHoraryStatement').click(function(){
   var id = $(this).attr('data');
  $('#btnDeleteHoraryStatement').attr("href",'/delete/horary-statement/'+id);
  $('#delete_horary_statement').modal();

});

 $('.editHoraryStatement').click(function(){
    var id = $(this).attr('data');
    getHoraryStatementToEdit(id);
    $('#add_horary_statement').modal();
 });

// Teaching

$('.deleteTeaching').click(function(){
   var id = $(this).attr('data');
  $('#btnDeleteTeaching').attr("href",'/delete/teaching/'+id);
  $('#delete_teaching').modal();
  //alert('id: '+ id);
});

 $('.editTeaching').click(function(){
    var id = $(this).attr('data');
    getTeachingToEdit(id);
    $('#add_teaching').modal();
 });

// Salary
 $('.editSalary').click(function(){
    var id = $(this).attr('data');
    getSalaryToEdit(id);
    $('#add_salary').modal();
 });

 $('#btnAddSalary').click(function(){
   $('#formAddSalary input').val('');
   $('#formAddSalary select').val('');
   $('.msg').addClass('hidden');
 });

 $('.deleteSalary').click(function(){
    var id = $(this).attr('data');
   $('#btnDeleteSalary').attr("href",'/delete/salary/'+id);
   $('#delete_salary').modal();
 });

 // SchoolPeriod
  $('.editSchoolPeriod').click(function(){
     var id = $(this).attr('data');
    // alert('id : '+ id);
     getSchoolPeriodToEdit(id);
     $('#add_school_period').modal();
  });

  $('#btnAddSchoolPeriod').click(function(){
    $('#formAddSchoolPeriod input').val('');
    $('#formAddSchoolPeriod select').val('');
    $('.msg').addClass('hidden');
  });

  $('.deleteSchoolPeriod').click(function(){
     var id = $(this).attr('data');
    $('#btnDeleteSchoolPeriod').attr("href",'/delete/school-period/'+id);
    $('#delete_school_period').modal();
  });

// User
$('#btnAddUser').click(function(){
  $('.AddUser').val('');
  $('.usernameHasError').addClass('hidden');
  $('.checkedSendMail').val('no');
});

 $('.editUser').click(function(){
    var id = $(this).attr('data');
    getUserToEdit(id);
    $('#get_user').modal();
 });

 $('.deleteUser').click(function(){
    var id = $(this).attr('data');
   $('#btnDeleteUser').attr('href','/delete/user/'+id);
   $('#delete_user').modal();
 });

  $('.activerUser').click(function(){
     var id = $(this).attr('data');
    $('#btnActiverUser').attr('href','/activer/user/'+id);
    $('#activer_user').modal();
  });

   $('.desactiverUser').click(function(){
       var id = $(this).attr('data');
      $('#btnDesactiverUser').attr('href','/desactiver/user/'+id);
      $('#desactiver_user').modal();
    });

   $('.checkedSendMail').change(function(){
        if($(this).is(":checked")){
          $(this).val('checked');
        }else{
          $(this).val('no');
        }
   });

//Cycle
 $('.editCycle').click(function(){
    var id = $(this).attr('data');
    getCycleToEdit(id);
    $('#add_cycle').modal();
    //alert('okkkkkk');
 });

  $('.deleteCycle').click(function(){
     var id = $(this).attr('data');
    $('#btnDeleteCycle').attr('href','/delete/cycle/'+id);
    $('#delete_cycle').modal();
  });

// Subject
 $('.editSubjects').click(function(){
    var id = $(this).attr('data');
    getSubjectsToEdit(id);
    $('#add_subjects').modal();

 });
  $('.deleteSubjects').click(function(){
      var id = $(this).attr('data');

      $('#btnDeleteSubjects').attr('href','/delete/subjects/'+id);
      $('#delete_subjects').modal();

   });

    // Classe

        $('.deleteClasse').click(function(){
                var id = $(this).attr('data');

                $('#btnDeleteClasse').attr('href','/delete/classe/'+id);
                $('#delete_classe').modal();

             });

              $('.editClasse').click(function(){
                 var id = $(this).attr('data');
                 getClasseToEdit(id);
                 $('#add_classe').modal();

              });

 $('.editUserPassword2').click(function(){
    var id = $(this).attr('data');
    $('.userId').val(id);
    $('#update_password').modal();
 });

 // Inscription
 $('.inscriptionNewStudent').click(function() {
     $('.oldStudentPart').addClass('hidden');
     $('.newStudentPart').removeClass('hidden');
     $('.studentTypeAddInscription').val(0);
     $('#btnSubmitAddInscription').removeClass('hidden');
  });

 $('.inscriptionOldStudent').click(function(){
    $('.oldStudentPart').removeClass('hidden');
    $('.newStudentPart').addClass('hidden');
    $('.studentTypeAddInscription').val(1);

    $('#add_inscription').modal();

    $('.msgSearchStudent').addClass('hidden');
    $('#btnSubmitAddInscription').addClass('hidden');
    $('.infoOldStudent').addClass('hidden');
    $('.numStudent').val('');
  });

 $('#checkStudent').click(function() {
    var num = $('.numStudent').val();
    getStudentByNumber(num);
 });

 $('.payPayment').click(function() {
   var studentId = $(this).attr('data');
   getPaymentByStudentId(studentId);
   $('#add_payment').modal();
 });

 $('.detailsPayment').click(function() {
    var id = $(this).attr('data');
    getPaymentById(id);
    $('#details_payment').modal();
 });

// Student mark
 $('#btnAddMark').click(function() {
    $('.markDateHasError').addClass('hidden');
    $('.markTypeHasError').addClass('hidden');
    $('.markValueHasError').addClass('hidden');
 });

 //edit student
  $('.editStudent').click(function(){
      var id = $(this).attr('data');
      getStudentToEdit(id);
      $('#update_student').modal();

   });

// edit student payment

 $('.editPayment').click(function(){
      var id = $(this).attr('data');
      getPaymentStudentToEdit(id);
      $('#add_payment').modal();

   });

 $('.deleteMark').click(function(){
      var id = $(this).attr('data');
     $('#btnDeleteMark').attr("href",'/delete/mark/'+id);
     $('#delete_mark').modal();
 });

 $('.editMark').click(function(){
     var id = $(this).attr('data');
     getMarkToEdit(id);
     $('#edit_mark').modal();
 });

 // Coefficient
 $('.editCoef').click(function(){
   var id = $(this).attr('data');
   getCoeffcientToEdit(id);
   $('#add_coef').modal();
 });

 $('#btnNewCoef').click(function(){
    $('.msg').addClass('hidden');
 });

 $('#btnNewCoef').click(function(){
    $('.classeMode1').removeClass('hidden');
    $('.classeMode2').addClass('hidden');
    $('.addCoef').val('');
 });

 // Inscription
 $('.editInscription').click(function(){
    var id = $(this).attr('data');
    getInscriptionToEdit(id);
    $('#edit_inscription').modal();
 });

 $('.detailsInscription').click(function(){
    var id = $(this).attr('data');
    getInscriptionView(id);
    $('#view_inscription').modal();
 });

// ******** Period *************

 $('.editPeriod').click(function(){
    var id = $(this).attr('data');
    getPeriodToEdit(id);
    //alert('okkk ::::'+ id);
    $('#add_period').modal();
 });

 // PRINT
 $('.classeBulletin').click(function(){
    var semester = $(this).attr('data');
    var schoolPeriodId = $('#schoolPeriodP').val();
    var classeId = $('#classeP').val();
    var size = $('#studentRowsSize').val();
   // alert('size : '+size);
  // printBulletin(classeId, semester, schoolPeriodId, size);
 });

// ----------------   END --------------------------------
});


function getStudentByNumber(num){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/student/num/"+ num,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                   if (data.id != null) {
                        $('.oldStudentIdAddInscription').val(data.id);
                        $('.nameOldStudent').val(data.firstName + ' ' + data.lastName);
                        $('.birthDateOldStudent').val(data.birthDate);

                        $('.infoOldStudent').removeClass('hidden');
                        $('#btnSubmitAddInscription').removeClass('hidden');
                        $('.msgSearchStudent').addClass('hidden');
                   } else {
                      $('.msgSearchStudent').removeClass('hidden');
                      $('.infoOldStudent').addClass('hidden');
                      $('#btnSubmitAddInscription').addClass('hidden');

                      $('.oldStudentIdAddInscription').val(null);
                   }
                },
                error: function (e) {
                 //  $('#btnSubmitAddMember').addClass('hidden');
                 $('.msgSearchStudent').removeClass('hidden');
                 $('.infoOldStudent').addClass('hidden');
                 $('#btnSubmitAddInscription').addClass('hidden');

                 $('.oldStudentIdAddInscription').val(null);
                }
            });
}

// FORMAT DATE
function formatDate(date) {
// Si le format de la date est DD/MM/YYYY
  /*  var sDate = date.split("/");
    var date2 = new Date(sDate[2],sDate[1]-1,sDate[0]);
*/
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day,month,year].join('-');
}

function getUserToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/user/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.userId').val(data.id);
                    $('.firstNameAddUser').val(data.firstName);
                    $('.lastNameAddUser').val(data.lastName);
                    $('.usernameAddUser').val(data.username);
                    $('.roleIdAddUser').val(data.roleId);

                },
                error: function (e) {
                   $('#btnSubmitAddUser').addClass('hidden');
                }
            });
}

function getCycleToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/cycle/"+id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.cycleId').val(data.id);
                    $('.nameAddCycle').val(data.name);
                    $('.descriptionAddCycle').val(data.description);

                   $('.titleUpdateCycle').removeClass('hidden');
                   $('.titleAddCycle').addClass('hidden');
                },
                error: function (e) {
                   $('#btnSubmitAddCycle').addClass('hidden');
                }
            });
}


function getSubjectsToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/subjects/"+id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.subjectsId').val(data.id);
                    $('.nameAddSubjects').val(data.name);
                    $('.descriptionAddSubjects').val(data.description);
                    $('.coefficientAddSubjects').val(data.coefficient);

                   $('.titleUpdateSubjects').removeClass('hidden');
                   $('.titleAddSubjects').addClass('hidden');
                },
                error: function (e) {
                   $('#btnSubmitAddSubjects').addClass('hidden');
                }
            });
}

function getSchoolPeriodToEdit(id){
   $.ajax({
             type: "GET",
             contentType: "application/json",
             url: "/get/school-period/"+ id,
             dataType: 'json',
             timeout: 100000,
             success: function (data) {
                 $('.schoolPeriodId').val(data.id);
                 $('.startYearAddSchoolPeriod').val(data.startYear);
                 $('.endYearAddSchoolPeriod').val(data.endYear);
                 $('.statusAddSchoolPeriod').val(data.status);
                 $('.beginDateAddSchoolPeriod').val(formatDate(data.beginDate));
                 $('.closeDateAddSchoolPeriod').val(formatDate(data.closeDate));

                 $('.titleUpdateSchoolPeriod').removeClass('hidden');
                 $('.titleAddSchoolPeriod').addClass('hidden');
             },
             error: function (e) {
                $('#btnSubmitAddSchoolPeriod').addClass('hidden');
             }
         });
}

function getProfessorToEdit(id){
   $.ajax({
             type: "GET",
             contentType: "application/json",
             url: "/get/professor/"+ id,
             dataType: 'json',
             timeout: 100000,
             success: function (data) {
                 $('.professorId').val(data.id);
                 $('.firstNameAddProfessor').val(data.firstName);
                 $('.lastNameAddProfessor').val(data.lastName);
                 $('.gradeAddProfessor').val(data.grade);
                 $('.phoneAddProfessor').val(data.phone);
                 $('.emailAddProfessor').val(data.email);
                 $('.salaryAddProfessor').val(data.salaryId);
                 $('.addressAddProfessor').val(data.address);

                 $('.titleUpdateProfessor').removeClass('hidden');
                 $('.titleAddProfessor').addClass('hidden');
             },
             error: function (e) {
                $('#btnSubmitAddProfessor').addClass('hidden');
             }
         });
}

function getSalaryToEdit(id){
   $.ajax({
             type: "GET",
             contentType: "application/json",
             url: "/get/salary/"+ id,
             dataType: 'json',
             timeout: 100000,
             success: function (data) {
                 $('.salaryId').val(data.id);
                 $('.amountAddSalary').val(data.amount);
                 $('.salaryTypeAddSalary').val(data.salaryType);

                 $('.titleUpdateSalary').removeClass('hidden');
                 $('.titleAddSalary').addClass('hidden');
             },
             error: function (e) {
                $('#btnSubmitAddSalary').addClass('hidden');
             }
         });
}


function getMaintenanceToEdit(id, type){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/maintenance/"+type+"/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.maintenanceId').val(data.id);
                    $('.typeAddMaintenance').val(data.type);
                    $('.vehicleAddMaintenance').val(data.vehicleId);
                    $('.startDateAddMaintenance').val(formatDate(data.startDate));
                    $('.endDateAddMaintenance').val(formatDate(data.endDate));

                    $('.titleUpdateMaintenance').removeClass('hidden');
                    $('.titleAddMaintenance').addClass('hidden');
                },
                error: function (e) {
                   $('#btnSubmitAddMaintenance').addClass('hidden');
                }
            });
}


function getClasseToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/classe/"+id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.classeId').val(data.id);
                    $('.nameAddClasse').val(data.name);
                    $('#levelAddClasse').val(data.level);
                    $('#selectCycle').val(data.cycleId);

                   $('.titleUpdateClasse').removeClass('hidden');
                   $('.titleAddClasse').addClass('hidden');
                },
                error: function (e) {
                   $('#btnSubmitAddClasse').addClass('hidden');
                }
            });
}

function getPaymentByStudentId(studentId){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/payment/student/"+ studentId,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.paymentId').val(data.id);
                        $('.studentId').val(data.studentId);
                        $('.monthAddPayment').val(data.month);
                        $('.amountAddPayment').val(data.amount);
                        $('.studentAddPayment').val(data.student);
                        $('.studentNumberAddPayment').val(data.studentNumber);
                        $('.classeAddPayment').val(data.classe);
                        $('.paymentDateAddPayment').val(formatDate(data.paymentDate));

                },
                error: function (e) {
                }
            });
}

function getPaymentById(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/payment/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.schoolPeriodPaymentView').val(data.schoolPeriod);
                        $('.monthPaymentView').val(data.month);
                        $('.paymentDatePaymentView').val(formatDate(data.paymentDate));
                        $('.amountPaymentView').val(data.amount + ' F CFA');
                        $('.paymentTypePaymentView').val(data.paymentType);
                        $('.studentPaymentView').val(data.student);
                        $('.studentNumberPaymentView').val(data.studentNumber);
                        $('.classePaymentView').val(data.classe);
                        $('.userPaymentView').val(data.user);
                        $('.creationDatePaymentView').val(data.creationDate);

                },
                error: function (e) {
                }
            });
}


function getStudentToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/student/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.studentId').val(data.id);
                        $('.firstNameStudent').val(data.firstName);
                        $('.lastNameStudent').val(data.lastName);
                        $('.birthDateStudent').val(formatDate(data.birthDate));
                        $('.birthPlaceStudent').val(data.birthPlace);
                        $('.addressStudent').val(data.address);
                        $('.phoneStudent').val(data.phone);
                        $('.emailStudent').val(data.email);
                        $('.tutorStudent').val(data.tutor);
                        $('.tutorAddressStudent').val(data.tutorAddress);
                        $('.tutorPhoneStudent').val(data.tutorPhone);

                        $('.originSchoolStudent').val(data.originSchool);
                        $('.addGender').val(data.gender);

                },
                error: function (e) {
                }
            });
}

function getMarkToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/mark/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.idEditMark').val(data.id);
                        $('.studentEditMark').text(data.student);
                        $('.studentNumberEditMark').text(data.studentNumber);
                        $('.markValueEditMark').val(data.markValue);
                        $('.schoolPeriodEditMark').text(data.schoolPeriod);
                        $('.subjectsEditMark').text(data.subjects);
                },
                error: function (e) {
                }
            });
}

function getCoeffcientToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/coefficient/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.coefficientId').val(data.id);
                        $('.subjectsId').val(data.subjectsId);
                        $('.coefAddCoef').val(data.coefValue);
                        $('.classeIdAddCoef').val(data.classeId);

                        $('.classeMode1').addClass('hidden');
                        $('.classeMode2').removeClass('hidden');
                },
                error: function (e) {
                }
            });
}

function getProfessorToPaid(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/new/payment/prof/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.professorName').text(data.professorName);
                        $('.professorId').val(data.professorId);
                        $('.monthAddProfPayment').val(data.month);
                        $('.totalHourAddProfPayment').val(data.totalHour);
                        $('.amountAddProfPayment').val(data.amount);
                        $('.balanceDueAddProfPayment').val(data.balanceDue);
                        $('.receivedAmountAddProfPayment').val(data.amount - data.balanceDue);
                },
                error: function (e) {
                }
            });
}

function getHoraryStatementToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/horary/statement/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.horaryStatementId').val(data.id);
                        $('.professorIdAddHoraryStatement').val(data.professorId);
                        $('.schoolPeriodAddHoraryStatement').val(data.schoolPeriod);
                        $('.horaryDateAddHoraryStatement').val(formatDate(data.horaryDate));
                        $('.monthAddHoraryStatement').val(data.month);
                        $('.hourNumberAddHoraryStatement').val(data.hourNumber);

                        $('.titleUpdateHoraryStatement').removeClass('hidden');
                        $('.titleAddHoraryStatement').addClass('hidden');

                },
                error: function (e) {
                }
            });
}

function getProfPayment(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/prof/payment/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.professorProfPayment').text(data.professor);
                        $('.schoolPeriodProfPayment').text(data.schoolPeriod);
                        $('.monthProfPayment').text(data.month);
                        $('.totalHourProfPayment').text(data.totalHour);
                        $('.amountProfPayment').text(data.amount);
                        $('.balanceDueProfPayment').text(data.balanceDue);
                        $('.receivedAmountProfPayment').text(data.amount - data.balanceDue);
                        $('.creationDateProfPayment').text(data.creationDate);
                        $('.paymentDateProfPayment').text(data.paymentDate);
                        $('.paymentTypeProfPayment').text(data.paymentType);
                        $('.userProfPayment').text(data.user);
                },
                error: function (e) {
                }
            });
}

function getTeachingToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/teaching/"+id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.teachingId').val(data.id);
                    $('.teachingProfessorId').val(data.professorId);
                    $('.schoolPeriodAddTeaching').val(data.schoolPeriod);
                    $('.subjectIdAddTeaching').val(data.subjectId);
                    $('.classeIdAddTeaching').val(data.classeId);
                    $('.teachingDayAddTeaching').val(data.teachingDay);
                    $('.startTeachingTimeAddTeaching').val(data.startTeachingTime);
                    $('.endTeachingTimeAddTeaching').val(data.endTeachingTime);



                   $('.titleUpdateTeaching').removeClass('hidden');
                   $('.titleAddTeaching').addClass('hidden');
                },
                error: function (e) {
                   $('#btnSubmitAddClasse').addClass('hidden');
                }
            });
}

function getPaymentStudentToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/payment/student/edit/"+id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.paymentIdToEdit').val(data.id);
                    $('.studentIdToEdit').val(data.studentId);
                    $('.monthAddPaymentToEdit').val(data.month);
                    $('.amountAddPaymentToEdit').val(data.amount);
                    $('.studentAddPaymentToEdit').val(data.student);
                    $('.studentNumberAddPaymentToEdit').val(data.studentNumber);
                    $('.classeAddPaymentToEdit').val(data.classe);
                    $('.paymentDateAddPaymentToEdit').val(formatDate(data.paymentDate));

                   $('.titleUpdateSalary').removeClass('hidden');
                   $('.titleAddSalary').addClass('hidden');
                },
                error: function (e) {
                   $('#btnSubmitAddPaymentStudent').addClass('hidden');
                }
            });
}

function getProfessorPaidToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/payment/prof/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.profPaymentId').val(data.id);
                        $('.professorIdToEdit').val(data.professorId);
                        $('.professorNameToEdit').text(data.professorName);
                        $('.monthAddProfPaymentToEdit').val(data.month);
                        $('.totalHourAddProfPaymentToEdit').val(data.totalHour);
                        $('.amountAddProfPaymentToEdit').val(data.amount);
                        $('.paymentDateAddProfPaymentToEdit').val(data.paymentDate);
                        $('.balanceDueAddProfPaymentToEdit').val(data.balanceDue);
                        $('.receivedAmountAddProfPaymentToEdit').val(data.amount - data.balanceDue);

                         $('.titleUpdateSalary').removeClass('hidden');
                         $('.titleAddSalary').addClass('hidden');
                },
                error: function (e) {
                }
            });
}

function getInscriptionToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/inscription/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.editInscriptionId').val(data.id);
                        $('.inscriptionDateEditInscription').val(formatDate(data.inscriptionDate));
                        $('.amountEditInscription').val(data.amount);
                        $('.classeIdEditInscription').val(data.classeId);
                        $('.studentNumberEditInscription').val(data.studentNumber);
                        $('.firstNameEditInscription').val(data.firstName);
                        $('.lastNameEditInscription').val(data.lastName);

                },
                error: function (e) {
                }
            });
}

function getInscriptionView(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/view/inscription/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                    $('.inscriptionDate').text(data.inscriptionDate);
                    $('.amountInscription').text(data.amount);
                    $('.studentInscription').text(data.firstName + ' ' + data.lastName);
                    $('.studentNumberInscription').text(data.studentNumber);
                    $('.birthDateInscription').text(data.birthDate);
                    $('.birthPlaceInscription').text(data.birthPlace);
                    $('.classeInscription').text(data.classe);
                    $('.userInscription').text(data.user);
                },
                error: function (e) {
                }
            });
}

function printBulletin(classeId, semester, schoolPeriodId, size){
       console.log('------- size = '+size);

         location.href = "/bulletin/classe/"+ classeId + "/" + semester + "/" + schoolPeriodId;
         if (size > 0){
           setTimeout(printBulletin(classeId, semester, schoolPeriodId, (size -1)), 10000);
         }
}


function getPeriodToEdit(id){
       $.ajax({
                type: "GET",
                contentType: "application/json",
                url: "/get/period/"+ id,
                dataType: 'json',
                timeout: 100000,
                success: function (data) {
                        $('.periodId').val(data.id);
                        $('.periodName').val(data.name);
                        $('#periodCycle').val(data.cycleId);
                        $('#periodType').val(data.periodType);

                        $('.titleUpdatePeriod').removeClass('hidden');
                        $('.titleAddPeriod').addClass('hidden');
                },
                error: function (e) {
                }
            });
}