package cms.der.sn.service.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import cms.der.sn.domain.entity.User;
import cms.der.sn.domain.repository.UserRepository;
import cms.der.sn.service.service.UserService;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {
  @Autowired
  private UserRepository userRepository;

  @Override
  @Secured("ROLE_ADMIN")
  public void save(User user) {
    userRepository.save(user);
  }

  @Override
  @Secured("ROLE_ADMIN")
  public void delete(Long id) {
    userRepository.delete(id);
  }

  @Override
  public List<User> findAll() {
    return userRepository.findAll();
  }

  @Override
  public User findById(Long id) {
    return userRepository.findOne(id);
  }

  @Override
  public List<User> findAllActifUsers() {
    return userRepository.findAllActifUsers();
  }

    @Override
    public User getCurrentUser() {
        User user=null;
        Object principal= SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal != null && principal instanceof UserDetails) {

            UserDetails userDetails=(UserDetails)principal;
            user=userRepository.findByLogin(userDetails.getUsername());
        }
        else{
            if (principal!=null){
                user=userRepository.findByLogin(principal.toString());
            }
        }
        return user;
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByLogin(username);
    }
}
