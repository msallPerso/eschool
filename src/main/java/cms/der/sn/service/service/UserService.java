package cms.der.sn.service.service;



import cms.der.sn.domain.entity.User;

import java.util.List;


public interface UserService {
  void save(User user);

  void delete(Long id);

  List<User> findAll();

  User findById(Long id);

  List<User> findAllActifUsers();

  User getCurrentUser();

  User findByUsername(String username);
}
