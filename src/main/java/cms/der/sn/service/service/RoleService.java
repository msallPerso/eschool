package cms.der.sn.service.service;



import cms.der.sn.domain.entity.Role;

import java.util.List;


public interface RoleService {
  Role findByName(String roleName);
  List<Role> findAll();

  Role findById(Long id);
}
