package cms.der.sn.service.service.impl;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;


public class GeneratePasswordService {
    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    public static String PASSWORD;

    private static SecureRandom random = new SecureRandom();

    public static String generatePassword(int len, String dic) {
        String result = "";
        for (int i = 0; i < len; i++) {
            int index = random.nextInt(dic.length());
            result += dic.charAt(index);
        }
        PASSWORD = result;
        return result;
    }
}
