package cms.der.sn.service.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import cms.der.sn.service.service.MailSenderService;

import javax.mail.internet.MimeMessage;
import java.io.File;


@Service
public class MailSenderServiceImpl implements MailSenderService {
    @Autowired
    private JavaMailSender javaMailSender;

    private static Logger LOGGER= LoggerFactory.getLogger(MailSenderServiceImpl.class);

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    @Override
    public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            FileSystemResource file
                    = new FileSystemResource(new File(pathToAttachment));
            helper.addAttachment("Invoice", file);

            javaMailSender.send(message);
        }catch (Exception ex) {
            LOGGER.error("####  "+ex.getMessage());
        }
    }

    @Override
    public String getMessageText(String username, String password) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Bonjour " + username + ",\n");
        stringBuilder.append("Votre compte d'utilisateur a été créé avec succès.");
        stringBuilder.append(" Vous pouvez à présent accéder à l'application smart school de ");

        stringBuilder.append("\n\n");
        stringBuilder.append("Voici vos identifiants : \n");
        stringBuilder.append("Nom d'utilisateur : " + username + "\n");
        stringBuilder.append("Mot de passe : "+password + "\n");
        stringBuilder.append("\n\n");
        stringBuilder.append("L'équipe admin");

        return stringBuilder.toString();
    }
}
