package cms.der.sn.service.service;


public interface MailSenderService {
    void sendSimpleMessage(String to, String subject, String text);

    void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment);

    String getMessageText(String username, String password);
}
