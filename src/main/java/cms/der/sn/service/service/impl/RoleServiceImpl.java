package cms.der.sn.service.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cms.der.sn.domain.entity.Role;
import cms.der.sn.domain.repository.RoleRepository;
import cms.der.sn.service.service.RoleService;

import java.util.List;


@Service
public class RoleServiceImpl implements RoleService {
  @Autowired
  private RoleRepository roleRepository;

  @Override
  public Role findByName(String roleName) {
    return roleRepository.findByName(roleName);
  }

  @Override
  public List<Role> findAll() {
    return roleRepository.findAll();
  }

  @Override
  public Role findById(Long id) {
    return roleRepository.findOne(id);
  }
}
