package cms.der.sn.service.mapper.impl;


import cms.der.sn.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.mapper.UserFormUpdatePasswordToUserMapper;
import cms.der.sn.service.model.user.UserFormUpdatePassword;

@Component
public class UserFormUpdatePasswordToUserMapperImpl implements UserFormUpdatePasswordToUserMapper {

    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public User apply(UserFormUpdatePassword userFormUpdatePassword) {

        User user=userService.findById(userFormUpdatePassword.getId());

       user.setPassword(bCryptPasswordEncoder.encode(userFormUpdatePassword.getNewPassword()));
        return user;
    }
}
