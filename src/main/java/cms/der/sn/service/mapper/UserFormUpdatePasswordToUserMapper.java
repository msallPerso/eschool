package cms.der.sn.service.mapper;



import cms.der.sn.domain.entity.User;
import cms.der.sn.service.model.user.UserFormUpdatePassword;

import java.util.function.Function;

public interface UserFormUpdatePasswordToUserMapper extends Function<UserFormUpdatePassword, User> {
}
