package cms.der.sn.service.mapper.impl;

import cms.der.sn.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.mapper.UserProfileToUserMapper;
import cms.der.sn.service.model.user.UserProfile;

import java.util.Date;
@Component
public class UserProfileToUserMapperImpl implements UserProfileToUserMapper {

    @Autowired
    private UserService userService;

    @Override
    public User apply(UserProfile userProfile) {

        User user= userService.findById(userProfile.getId());
        if(userProfile.getId()!=null){

            //user.setFirstName(userProfile.getFirstName());
           // user.setLastName(userProfile.getLastName());
            user.setAddress(userProfile.getAddress());
            user.setEmail(userProfile.getEmail());
            user.setPhone(userProfile.getPhone());
            user.setLastUpdate(new Date());


        }
        return user;
    }
}
