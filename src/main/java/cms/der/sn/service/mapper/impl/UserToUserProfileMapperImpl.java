package cms.der.sn.service.mapper.impl;


import org.springframework.stereotype.Component;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.mapper.UserToUserProfileMapper;
import cms.der.sn.service.model.user.UserProfile;

@Component
public class UserToUserProfileMapperImpl implements UserToUserProfileMapper {
    @Override
    public UserProfile apply(User user) {
        UserProfile userProfile=new UserProfile();

        userProfile.setId(user.getId());
        userProfile.setFirstName(user.getFirstName());
        userProfile.setLastName(user.getLastName());
        userProfile.setAddress(user.getAddress());
        userProfile.setEmail(user.getEmail());
        userProfile.setPhone(user.getPhone());
        userProfile.setUserName(user.getUsername());
        return userProfile;
    }
}
