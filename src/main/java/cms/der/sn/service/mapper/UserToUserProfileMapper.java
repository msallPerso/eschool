package cms.der.sn.service.mapper;


import cms.der.sn.domain.entity.User;
import cms.der.sn.service.model.user.UserProfile;

import java.util.function.Function;

public interface UserToUserProfileMapper extends Function<User,UserProfile> {
}
