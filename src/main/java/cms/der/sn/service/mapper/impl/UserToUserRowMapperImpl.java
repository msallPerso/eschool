package cms.der.sn.service.mapper.impl;


import org.springframework.stereotype.Component;
import cms.der.sn.domain.entity.Role;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.mapper.UserToUserRowMapper;
import cms.der.sn.service.model.user.UserRow;


@Component
public class UserToUserRowMapperImpl implements UserToUserRowMapper {
  @Override
  public UserRow apply(User user) {
    UserRow userRow = new UserRow();
    userRow.setId(user.getId());
    userRow.setFirstName(user.getFirstName());
    userRow.setLastName(user.getLastName());
    userRow.setUsername(user.getUsername());
    userRow.setCreationDate(user.getCreationDate());
    userRow.setAccountState(user.getAccountState());
    String roleName="";
    int i = 0;
    for (Role role : user.getRoleList()){
      if (i == 0){
        roleName = roleName+role.getName();
      }else {
        roleName = roleName+", "+role.getName();
      }
      i++;
    }
    userRow.setRoleName(roleName);

    return userRow;
  }
}
