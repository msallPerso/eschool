package cms.der.sn.service.mapper.impl;


import cms.der.sn.service.service.RoleService;
import cms.der.sn.service.service.UserService;
import cms.der.sn.service.service.impl.GeneratePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import cms.der.sn.domain.entity.Role;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.mapper.UserFormToUserMapper;
import cms.der.sn.service.model.user.UserForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class UserFormToUserMapperImpl implements UserFormToUserMapper {
  @Autowired
  private UserService userService;
  @Autowired
  private RoleService roleService;
  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Override
  public User apply(UserForm userForm) {
    User user;
    if (userForm.getId() != null){
      user = userService.findById(userForm.getId());
    }else{
      user = new User();
      user.setCreationDate(new Date());
      String password;
      if (userForm.getSendMail()!=null && userForm.getSendMail().equals("checked")){
         password = GeneratePasswordService.generatePassword(12, GeneratePasswordService.alphanum);
      } else {
        password =userForm.getUsername().toLowerCase();
      }
      user.setPassword(bCryptPasswordEncoder.encode(password));
      user.setStatus(User.ACTIF);
      user.setAccountState(User.ETAT_COMPTE_ACTIVER);
    }
    user.setLastUpdate(new Date());
    user.setFirstName(userForm.getFirstName());
    user.setLastName(userForm.getLastName());
    user.setUsername(userForm.getUsername());
    user.setEmail(userForm.getEmail());
    user.setPhone(userForm.getPhone());
    user.setAddress(userForm.getAddress());

    List<Role> roles = new ArrayList<>();
    Role role = roleService.findById(userForm.getRoleId());
    roles.add(role);
    user.setRoleList(roles);

    return user;
  }
}
