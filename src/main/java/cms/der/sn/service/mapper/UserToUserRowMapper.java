package cms.der.sn.service.mapper;



import cms.der.sn.domain.entity.User;
import cms.der.sn.service.model.user.UserRow;

import java.util.function.Function;


public interface UserToUserRowMapper extends Function<User, UserRow> {
}
