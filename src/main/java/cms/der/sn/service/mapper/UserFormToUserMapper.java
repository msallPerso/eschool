package cms.der.sn.service.mapper;



import cms.der.sn.domain.entity.User;
import cms.der.sn.service.model.user.UserForm;

import java.util.function.Function;


public interface UserFormToUserMapper extends Function<UserForm, User> {
}
