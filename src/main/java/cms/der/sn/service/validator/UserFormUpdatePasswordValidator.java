package cms.der.sn.service.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.model.user.UserFormUpdatePassword;
import cms.der.sn.service.service.UserService;

@Component
public class UserFormUpdatePasswordValidator implements Validator{

    private final String LASTPASSWORD="lastPassword";
    private final String NEWPASSWORD="newPassword";
    private final String CONFIRMPASSWORD="confirmPassword";

    private static final String EMPTY_FIELD_ERROR_CODE="546";
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public boolean supports(Class<?> aClass) {
        return UserFormUpdatePassword.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        UserFormUpdatePassword userFormUpdatePassword = (UserFormUpdatePassword) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,LASTPASSWORD,EMPTY_FIELD_ERROR_CODE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,NEWPASSWORD,EMPTY_FIELD_ERROR_CODE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,CONFIRMPASSWORD,EMPTY_FIELD_ERROR_CODE);

        User user=userService.findById(userFormUpdatePassword.getId());
       /* if (!user.getPassword().equals(bCryptPasswordEncoder.encode(userFormUpdatePassword.getLastPassword()))){
            errors.rejectValue(LASTPASSWORD,EMPTY_FIELD_ERROR_CODE);
        } */
        if (! bCryptPasswordEncoder.matches(userFormUpdatePassword.getLastPassword(), user.getPassword())){
            errors.rejectValue(LASTPASSWORD,EMPTY_FIELD_ERROR_CODE);
        }
        if (!userFormUpdatePassword.getNewPassword().equals(userFormUpdatePassword.getConfirmPassword())){
            errors.rejectValue(CONFIRMPASSWORD,EMPTY_FIELD_ERROR_CODE);
        }
    }
}
