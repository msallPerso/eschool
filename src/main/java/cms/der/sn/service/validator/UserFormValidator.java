package cms.der.sn.service.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.model.user.UserForm;
import cms.der.sn.service.service.UserService;


@Component
public class UserFormValidator implements Validator {
    private final String FIRST_NAME="firstName";
    private final String LAST_NAME="lastName";
    private final String USERNAME="username";
    private final String ROLE_ID="roleId";

    private static final String EMPTY_FIELD_ERROR_CODE="500";

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserForm userForm = (UserForm) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,FIRST_NAME,EMPTY_FIELD_ERROR_CODE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,LAST_NAME,EMPTY_FIELD_ERROR_CODE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,USERNAME,EMPTY_FIELD_ERROR_CODE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,ROLE_ID,EMPTY_FIELD_ERROR_CODE);

        User user = userService.findByUsername(userForm.getUsername());
        if (user != null && userForm.getId()== null){
            errors.rejectValue(USERNAME, EMPTY_FIELD_ERROR_CODE);
        }

    }
}
