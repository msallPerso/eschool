package cms.der.sn.service.model.user;

import lombok.Data;

import java.util.List;

@Data
public class UserList {
  private List<UserRow> rows;

  public UserList() {
  }

  public UserList(List<UserRow> rows) {
    this.rows = rows;
  }
}
