package cms.der.sn.service.model.user;

import lombok.Data;

@Data
public class UserFormUpdatePassword {
    private Long id;
    private String lastPassword;
    private String newPassword;
    private String confirmPassword;

    public UserFormUpdatePassword() {
    }

    public UserFormUpdatePassword(Long id) {
        this.id = id;
    }

    public UserFormUpdatePassword(Long id, String lastPassword, String newPassword, String confirmPassword) {
        this.id = id;
        this.lastPassword = lastPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }
}
