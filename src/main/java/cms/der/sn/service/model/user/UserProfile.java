package cms.der.sn.service.model.user;

import lombok.Data;

@Data
public class UserProfile {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private String phone;
    private String userName;

    public UserProfile() {
    }

    public UserProfile(Long id, String firstName, String lastName, String email, String address, String phone, String userName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.userName = userName;
    }
}
