package cms.der.sn.service.model.user;

import lombok.Data;


@Data
public class UserForm {
  private Long id;
  private String firstName;
  private String lastName;
  private String username;
  private String email;
  private String address;
  private String phone;
  private String roleName;
  private Long roleId;
  private String sendMail;

  public UserForm() {
  }

  public UserForm(Long id, String firstName, String lastName, String username) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.username = username;
  }
}
