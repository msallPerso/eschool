package cms.der.sn.service.model.user;

import lombok.Data;

import java.util.Date;

@Data
public class UserRow {
  private Long id;
  private String firstName;
  private String lastName;
  private String username;
  private Date creationDate;
  private String roleName;
  private String accountState;

  public UserRow() {
  }
}
