package cms.der.sn.service.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import cms.der.sn.view.CustomView;


@Controller
public class LoginController {

  @RequestMapping(value = {"/","/index"}, method = RequestMethod.GET)
  public String index(){

    return CustomView.INDEX;
  }

  @RequestMapping(value = "/access-denied")
  public String accessDenied(){
    return CustomView.ACCESS_DENIED;
  }
}
