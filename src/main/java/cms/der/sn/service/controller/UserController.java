package cms.der.sn.service.controller;


import cms.der.sn.service.mapper.*;
import cms.der.sn.service.model.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import cms.der.sn.domain.entity.Role;
import cms.der.sn.domain.entity.User;
import cms.der.sn.service.service.MailSenderService;
import cms.der.sn.service.service.RoleService;
import cms.der.sn.service.service.UserService;
import cms.der.sn.service.service.impl.GeneratePasswordService;
import cms.der.sn.service.validator.UserFormUpdatePasswordValidator;
import cms.der.sn.service.validator.UserFormValidator;
import cms.der.sn.view.CustomView;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Controller
public class UserController {
  private final UserToUserRowMapper userToUserRowMapper;
  private final UserService userService;
  private final RoleService roleService;
  private final UserFormToUserMapper userFormToUserMapper;
  private final UserProfileToUserMapper userProfileToUserMapper;
  private final UserToUserProfileMapper userToUserProfileMapper;
  private final UserFormUpdatePasswordToUserMapper userFormUpdatePasswordToUserMapper;
  private final UserFormUpdatePasswordValidator userFormUpdatePasswordValidator;
  private final UserFormValidator userFormValidator;
  private final MailSenderService mailSenderService;

  private UserForm userForm;
  private UserFormUpdatePassword userFormUpdatePassword;

  private  static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @Autowired
  public UserController(UserToUserRowMapper userToUserRowMapper,
                        UserService userService,
                        RoleService roleService,
                        UserFormToUserMapper userFormToUserMapper,
                        UserProfileToUserMapper userProfileToUserMapper,
                        UserToUserProfileMapper userToUserProfileMapper,
                        UserFormUpdatePasswordToUserMapper userFormUpdatePasswordToUserMapper,
                        UserFormUpdatePasswordValidator userFormUpdatePasswordValidator,
                        UserFormValidator userFormValidator,
                        MailSenderService mailSenderService){
    this.userToUserRowMapper = userToUserRowMapper;
    this.userService = userService;
    this.roleService = roleService;
    this.userFormToUserMapper = userFormToUserMapper;
    this.userProfileToUserMapper = userProfileToUserMapper;
    this.userToUserProfileMapper = userToUserProfileMapper;
    this.userFormUpdatePasswordToUserMapper = userFormUpdatePasswordToUserMapper;
    this.userFormUpdatePasswordValidator = userFormUpdatePasswordValidator;
    this.userFormValidator = userFormValidator;
    this.mailSenderService = mailSenderService;
  }

  @RequestMapping(value = "/users", method = RequestMethod.GET)
  public String findAllUsers(Model model){
    List<User> users = userService.findAllActifUsers();
    List<UserRow> rows = users.stream()
            .map(userToUserRowMapper)
            .collect(Collectors.toList());
    UserList userList = new UserList(rows);

    if (userForm == null){
      userForm = new UserForm();
    }
    model.addAttribute("userList", userList);
    model.addAttribute("userForm", userForm);
    model.addAttribute("roles", roleService.findAll());

    User user=userService.getCurrentUser();
    model.addAttribute("userCompte", user);

    return CustomView.USER_LIST;
  }

  @RequestMapping(value = "/add/user", method = RequestMethod.POST)
  public String addUser(@ModelAttribute("userForm") UserForm userForm, RedirectAttributes redirectAttributes,
                        BindingResult result){
    userFormValidator.validate(userForm, result);
    if (result.hasErrors()){
      redirectAttributes.addFlashAttribute("isAddUser", "true");
      this.userForm = userForm;
      if (result.hasFieldErrors("username")){
        redirectAttributes.addFlashAttribute("usernameHasError", "true");
      }
    } else {
      User user = Optional.of(userForm)
              .map(userFormToUserMapper)
              .get();
      userService.save(user);
      redirectAttributes.addFlashAttribute("msgAddUser", "true");
      if (userForm.getSendMail()!=null && userForm.getSendMail().equals("checked")){
        mailSenderService.sendSimpleMessage(user.getEmail(), "SMART SCHOOL", mailSenderService.getMessageText(user.getUsername(), GeneratePasswordService.PASSWORD));
      }
    }
    return CustomView.REDIRECT_USER_LIST;
  }

  @RequestMapping(value = "/get/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody UserForm getUser(@PathVariable Long id){
    User user = userService.findById(id);
    UserForm userForm = null;
    try {
      userForm = new UserForm(user.getId(), user.getFirstName(), user.getLastName(), user.getUsername());
      userForm.setRoleName(user.getRoleList().get(0).getName());
      userForm.setRoleId(user.getRoleList().get(0).getId());
    }catch (Exception ex){
     LOGGER.error("*********  "+ex.getMessage());
    }
    LOGGER.error("*********  "+userForm);
    return userForm;
  }

    @RequestMapping(value = "/edit/user", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("userForm") UserForm userForm, RedirectAttributes redirectAttributes, BindingResult result){

            User user = Optional.of(userForm)
                    .map(userFormToUserMapper)
                    .get();
            userService.save(user);
            redirectAttributes.addFlashAttribute("msgEditUser", "true");

        LOGGER.error("*********  "+userForm);

        return CustomView.REDIRECT_USER_LIST;
    }


    @RequestMapping(value = "/delete/user/{id}", method = RequestMethod.GET)
  public String deleteUser(@PathVariable Long id, RedirectAttributes redirectAttributes){
    User user = userService.findById(id);

    Role role = null;
    try {
      role = user.getRoleList().stream()
              .filter(r -> r.getName().compareTo("ROLE_ADMIN") == 0)
              .findFirst()
              .get();
      redirectAttributes.addFlashAttribute("msgDeleteAdmin","L'administrateur ne peut pas être supprimé !");
    }catch (Exception ex){
      LOGGER.error("L'utilisateur à supprimer n'est pas un administrateur : "+ex.getMessage());
    }
    if (role == null) {
      user.setStatus(User.INACTIF);
      userService.save(user);
      redirectAttributes.addFlashAttribute("msgDeleteUser", "true");
    }

    return CustomView.REDIRECT_USER_LIST;
  }

  @RequestMapping(value = "/desactiver/user/{id}", method = RequestMethod.GET)
  public String desactiverCompte(@PathVariable Long id, RedirectAttributes redirectAttributes){
    User user = userService.findById(id);

    if(user!=null) {
      user.setStatus(User.DESACTIVER_COMPTE);
      user.setAccountState(User.ETAT_COMPTE_DESACTIVER);
      userService.save(user);
      redirectAttributes.addFlashAttribute("msgDesactiverUser", "true");
    }

    return CustomView.REDIRECT_USER_LIST;
  }
  @RequestMapping(value = "/activer/user/{id}", method = RequestMethod.GET)
  public String activerCompte(@PathVariable Long id, RedirectAttributes redirectAttributes){
    User user = userService.findById(id);

    if(user!=null) {
      user.setStatus(User.ACTIF);
      user.setAccountState(User.ETAT_COMPTE_ACTIVER);
      userService.save(user);
      redirectAttributes.addFlashAttribute("msgActiverUser", "true");
    }

    return CustomView.REDIRECT_USER_LIST;
  }

// Mon compte
  @RequestMapping(value = "/mon-compte", method = RequestMethod.GET)
  public String monCompte(Model model){

    User user=userService.getCurrentUser();
    UserProfile userProfile=Optional.of(user)
            .map(userToUserProfileMapper)
            .get();

    if (userFormUpdatePassword==null){
      userFormUpdatePassword=new UserFormUpdatePassword(userProfile.getId());
    }

    model.addAttribute("userProfile", userProfile);
    model.addAttribute("userFormUpdatePassword",userFormUpdatePassword);

    return CustomView.MON_COMPTE;
  }

  @RequestMapping(value = "/update/compte", method = RequestMethod.POST)
  public String updateCompte(@ModelAttribute("userProfile") UserProfile userProfile,RedirectAttributes redirectAttributes){
    User user = Optional.of(userProfile)
            .map(userProfileToUserMapper)
            .get();
    userService.save(user);

    redirectAttributes.addFlashAttribute("msgUpdateCompte","true");


    return CustomView.REDIRECT_MON_COMPTE;
  }

  @RequestMapping(value = "/update/password", method = RequestMethod.POST)
  public String updatePassword(@ModelAttribute("userFormUpdatePassword") UserFormUpdatePassword userFormUpdatePassword, BindingResult result, RedirectAttributes redirectAttributes){

    System.out.println("debbug   "+userFormUpdatePassword);

    userFormUpdatePasswordValidator.validate(userFormUpdatePassword,result);
    if (result.hasErrors()){
      this.userFormUpdatePassword = userFormUpdatePassword;
      if (result.hasFieldErrors("lastPassword")){
        redirectAttributes.addFlashAttribute("hasFieldErrorsLastPassword", "true");
      }

      if (result.hasFieldErrors("confirmPassword")){
        redirectAttributes.addFlashAttribute("hasFieldErrorsConfirmPassword", "true");
      }
    }
    else {
      User user = Optional.of(userFormUpdatePassword)
              .map(userFormUpdatePasswordToUserMapper)
              .get();
      userService.save(user);

      redirectAttributes.addFlashAttribute("msgUpdatePassword", "true");
    }


    return CustomView.REDIRECT_MON_COMPTE;
  }




}
