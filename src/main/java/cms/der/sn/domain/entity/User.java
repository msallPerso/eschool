package cms.der.sn.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "\"user\"")
public class User implements Serializable {
 // private static final String SEQ_GENERATOR_NAME="userSeq";
  @Id
  //@SequenceGenerator(name = SEQ_GENERATOR_NAME, sequenceName = "SFINX_USER_SEQ", allocationSize = 1, initialValue = 1)
  //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_GENERATOR_NAME)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column(name = "username", length = 60, unique = true)
  private String username;
  @Column(name = "password", length = 200)
  private String password;
  @Column(name = "first_name", length = 60)
  private String firstName;
  @Column(name = "last_name", length = 60)
  private String lastName;
  @Column(name = "creation_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;
  @Column(name = "last_update")
  @Temporal(TemporalType.TIMESTAMP)
  private Date lastUpdate;
  @Column(name = "status")
  private int status;

  @Column(name = "account_state")
  private String accountState;

  @Column(name = "email")
  private String email;
  @Column(name = "phone")
  private String phone;
  @Column(name = "address")
  private String address;


  public static final String ETAT_COMPTE_ACTIVER="ACTIF";
  public static final String ETAT_COMPTE_DESACTIVER="INACTIF";
  @ManyToMany
  @JoinTable(name = "user_role", joinColumns = {
          @JoinColumn(name = "user_id", referencedColumnName = "id")}, inverseJoinColumns = {
          @JoinColumn(name = "role_id", referencedColumnName = "id")})
  private List<Role> roleList;

  public User(){}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public List<Role> getRoleList() {
    return roleList;
  }

  public void setRoleList(List<Role> roleList) {
    this.roleList = roleList;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getAccountState() {
    return accountState;
  }

  public void setAccountState(String accountState) {
    this.accountState = accountState;
  }

  public static final int ACTIF=1;
  public static final int INACTIF=0;
  public static final int DESACTIVER_COMPTE=2;

  @Override
  public String toString() {
    return "User{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", creationDate=" + creationDate +
            ", lastUpdate=" + lastUpdate +
            ", status=" + status +
            ", email='" + email + '\'' +
            ", phone='" + phone + '\'' +
            ", address='" + address + '\'' +
            ", roleList=" + roleList +
            '}';
  }
}
