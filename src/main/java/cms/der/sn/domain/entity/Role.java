package cms.der.sn.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "role")
public class Role implements Serializable {
  //private static final String SEQ_GENERATOR_NAME="roleSeq";
  @Id
  //@SequenceGenerator(name = SEQ_GENERATOR_NAME, sequenceName = "SFINX_ROLE_SEQ", allocationSize = 1, initialValue = 1)
  //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ_GENERATOR_NAME)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column(name = "name", length = 40)
  private String name;
  @Column(name = "description", length = 100)
  private String description;

  public static final int ACTIF=1;
  public static final int INACTIF=0;

  @ManyToMany(mappedBy = "roleList")
  private List<User> userList;

  public Role(){}

  public Role(Long id, String name, String description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<User> getUserList() {
    return userList;
  }

  public void setUserList(List<User> userList) {
    this.userList = userList;
  }

  @Override
  public String toString() {
    return "Role{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            '}';
  }
}
