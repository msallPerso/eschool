package cms.der.sn.domain.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import cms.der.sn.domain.entity.Role;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query("Select r FROM Role r WHERE r.name = :x")
    Role findByName(@Param("x") String roleName);
}
