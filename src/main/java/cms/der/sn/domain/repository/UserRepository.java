package cms.der.sn.domain.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import cms.der.sn.domain.entity.User;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("Select u from User u WHERE u.status IN (1,2)")
    List<User> findAllActifUsers();

    @Query("Select u FROM User u WHERE u.username = :x")
    User findByLogin(@Param("x") String login);

    //@Query("SELECT u FROM User u WHERE u.username = :x AND u.password = :y")
    // User findByLoginAndPassword(@Param("x") String login, @Param("y") String password);
}
