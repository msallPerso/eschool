package cms.der.sn;


import cms.der.sn.domain.entity.Role;
import cms.der.sn.domain.entity.User;
import cms.der.sn.domain.repository.RoleRepository;
import cms.der.sn.domain.repository.UserRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
//import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EnableAutoConfiguration
@SpringBootApplication
@EntityScan("cms.der.sn.domain.entity")
@EnableJpaRepositories("cms.der.sn.domain.repository")
public class DerApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(DerApplication.class, args);
        RoleRepository roleRepository = ctx.getBean(RoleRepository.class);
        UserRepository userRepository = ctx.getBean(UserRepository.class);
        BCryptPasswordEncoder bCryptPasswordEncoder = ctx.getBean(BCryptPasswordEncoder.class);
        init(roleRepository, userRepository, bCryptPasswordEncoder);
    }

    public static void init(RoleRepository roleRepository, UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder){
        List<Role> roles = roleRepository.findAll();
        Role admin = null;
        if (roles == null || roles.isEmpty()){
            admin = new Role();
            admin.setName("ROLE_ADMIN");
            admin.setDescription("Administrateur");
            roleRepository.save(admin);
            Role role = new Role();
            role.setName("ROLE_USER");
            role.setDescription("Simple utilisateur");
            roleRepository.save(role);
        }
        List<User> users = userRepository.findAll();
        if (users == null || users.isEmpty()){
            User user = new User();
            user.setFirstName("Admin");
            user.setLastName("Admin");
            user.setCreationDate(new Date());
            user.setLastUpdate(new Date());
            user.setUsername("admin");
            user.setStatus(1);
            user.setAccountState("ACTIF");
            List<Role> roleList = new ArrayList<>(); roleList.add(admin);
            user.setRoleList(roleList);
            user.setPassword(bCryptPasswordEncoder.encode("admin"));
            userRepository.save(user);
        }
    }
}
