package cms.der.sn.view;


public class CustomView {
  public static final String INDEX="index";
  public static final String ACCESS_DENIED="access_denied";


  public static final String SCHOOL_PERIOD_LIST="school_period/school_period_list";
  public static final String REDIRECT_SCHOOL_PERIOD_LIST="redirect:/school-periods";


  public static final String USER_LIST="user/user_list";
  public static final String REDIRECT_USER_LIST="redirect:/users";

  public static final String CYCLE_LIST="cycle/cycle_list";
  public static final String CYCLE_DETAILS="/cycle/details_cycle";
  public static final String REDIRECT_CYCLE_DETAILS="redirect:/details/cycle/";
  public static final String REDIRECT_CYCLE_LIST="redirect:/cycles";

  public static final String CLASSE_LIST="classe/classe_list";
  //public static final String CLASSE_DETAILS="/classe/details";
  public static final String REDIRECT_CLASSE_LIST="redirect:/classes";

  public static final String SUBJECTS_LIST="subjects/subjects_list";
  public static final String SUBJECTS_DETAILS="subjects/details";
  public static final String REDIRECT_SUBJECTS_LIST="redirect:/subjects";
  public static final String REDIRECT_SUBJECTS_DETAILS="redirect:/details/subject/";


  public static final String MON_COMPTE="profile/mon_compte";
  public static final String REDIRECT_MON_COMPTE="redirect:/mon-compte";

  public static final String SETTINGS="settings/settings";
  public static final String REDIRECT_SETTINGS="redirect:/settings";


  public static final String PARENT_LIST="parent/parent_list";
  public static final String REDIRECT_ELEVE_LIST="redirect:/eleves";
  public static final String ELEVE_LIST="eleve/eleve_list";


}
